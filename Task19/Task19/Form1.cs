﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task19.Model;

namespace Task19
{

    public partial class Form1 : Form
    {
        private slNameDBContext slNameContext;
        //this object represents the current state of the DB


        public Form1()
        {
            InitializeComponent();
            slNameContext = new slNameDBContext();
            List<Supervisor> supervisors = slNameContext.Supervisors.ToList();

            foreach (Supervisor item in supervisors)
            {
                //add each to some component

                Supervisorcombobox.Items.Add(item);
            }
            RefreshTable();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            // Fra slides
            //new object for EF to use in a DBSet
            Student student = new Student
            {
                Name = SelectedStudentName.Text,
                SupervisorId = ((Supervisor) Supervisorcombobox.SelectedItem).Id
            };
            slNameContext.Students.Add(student);
            slNameContext.SaveChanges();
            RefreshTable();
        }
        private void RefreshTable()
        {
            // this allows us to tie a DGV to a given source
            BindingSource myBindingSource = new BindingSource();
            dataGridView1.DataSource = myBindingSource;
            myBindingSource.DataSource =slNameContext.Students.ToList();
            //it will accept collection sources stick to list for now
            dataGridView1.Refresh();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            // find the object in the DBSet you want to delete
            int selectedid = int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            Student tempObj = slNameContext.Students.Find(selectedid);
            //Remove it
            slNameContext.Students.Remove(tempObj);
            slNameContext.SaveChanges();
            RefreshTable();
        }
    }
}
