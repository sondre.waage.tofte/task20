namespace Task19.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatabasestructure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SupervisorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Supervisors", t => t.SupervisorId, cascadeDelete: true)
                .Index(t => t.SupervisorId);
            
            CreateTable(
                "dbo.Supervisors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.Coaches");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Coaches",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            DropForeignKey("dbo.Students", "SupervisorId", "dbo.Supervisors");
            DropIndex("dbo.Students", new[] { "SupervisorId" });
            DropTable("dbo.Supervisors");
            DropTable("dbo.Students");
        }
    }
}
